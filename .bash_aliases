alias mkdir='mkdir -p'

alias apts='apt-cache search'
alias apti='sudo apt-get install --install-suggests --assume-yes'

alias servethis='http-server . &> /dev/null &'

alias xo='xdg-open'

alias killchrome='sudo killall -9 chrome'
alias takenote='mkdir -p ~/notes; vim ~/notes/notes-`date +%d-%m-%Y`.md'
alias takenotes='mkdir -p ~/notes; vim ~/notes/notes-`date +%d-%m-%Y`.md'
alias takefic='mkdir -p ~/fiction; vim ~/fiction/fiction-`date +%d-%m-%Y`.md'
alias dirmap="tree -L 1 -I '.git|node_modules|tmp'"

alias prettyjson="jq -C '.'"

alias spacetake="du -chs | tail -n1"

function fpbp() { # find process using TCP port
  sudo ss -ltpn "sport = :$1";
}

function shot() {
  scrot --silent --quality 100 -e 'mv $f ~/screenshots'
}

function lowshot() {
  scrot --silent --quality 80 -e 'mv $f ~/screenshots'
}
